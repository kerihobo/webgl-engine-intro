import { WebGLApp } from "./webGLApp";

var canvas  : HTMLCanvasElement      = <HTMLCanvasElement>document.getElementById("canvas");
var gl      : WebGL2RenderingContext = <WebGL2RenderingContext>canvas.getContext("webgl2");
var webGLApp: WebGLApp               = new WebGLApp();

export const MAIN = {
    canvas
,   gl
,   webGLApp
}

webGLApp.initialize();