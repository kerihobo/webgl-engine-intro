import { mat4, vec3, vec2 } from "gl-matrix";
import { MAIN } from "./main";
import { Input } from "./input";
import { Shader } from "./shaders/shader";
import { ShaderObject } from "./shaders/shaderObject";
import { MathUtils } from "./utils/mathUtils";

export class Camera {
    constructor() {
        this.update();
    }
    
    projectionMatrix : mat4   = mat4.create();
    viewMatrix       : mat4   = mat4.create();
    fovRadians       : number = MathUtils.DEG_TO_RAD(60);
    aspect           : number = MAIN.canvas.clientWidth / MAIN.canvas.clientHeight;
    near             : number = 1;
    far              : number = 200;
    moveSpeed        : number = 0.5; 
    rotateSpeed      : number = 0.005; 
    pitch            : number = MathUtils.DEG_TO_RAD(-50);
    yaw              : number = MathUtils.DEG_TO_RAD(-90);
    position         : vec3   = vec3.fromValues(0, 70, 90);
    target           : vec3   = vec3.create();
    lookDirection    : vec3   = vec3.create();

    update(): void {
        if (MAIN.canvas.width != MAIN.canvas.clientWidth || MAIN.canvas.height != MAIN.canvas.clientHeight) {
            this.updateViewport();
        }

        this.updateProjectionMatrix();
        this.updateViewMatrix();
    
        this.updateShader3vf(MAIN.webGLApp.shaderObject);
        this.updateShader3vf(MAIN.webGLApp.shaderBullet);
        this.updateShaderUniform(MAIN.webGLApp.shaderGrid);
        this.updateShaderUniform(MAIN.webGLApp.shaderObject);
        this.updateShaderUniform(MAIN.webGLApp.shaderBullet);
        this.updateShaderUniform(MAIN.webGLApp.shaderLightGizmo);
    }

    updateShaderUniform(_shader: Shader) {
        MAIN.gl.useProgram(_shader.program);
        MAIN.gl.uniformMatrix4fv(_shader.propertyLocationProjectionMatrix, false, this.projectionMatrix);
        MAIN.gl.uniformMatrix4fv(_shader.propertyLocationViewMatrix, false, this.viewMatrix);
    }

    updateShader3vf(_shader: Shader) {
        MAIN.gl.useProgram(_shader.program);
        MAIN.gl.uniform3fv((<ShaderObject>_shader).propertyLocationCameraPosition, this.position);
    }

    updateProjectionMatrix(): void {
        var aspect = MAIN.canvas.clientWidth / MAIN.canvas.clientHeight;

        mat4.perspective(this.projectionMatrix, this.fovRadians, aspect, this.near, this.far);
    }
    
    updateViewMatrix(): void {
        this.rotateCamera();
        this.moveCamera();
        
        vec3.add(this.target, this.position, this.lookDirection);
        mat4.lookAt(this.viewMatrix, this.position, this.target, vec3.fromValues(0, 1, 0));
    }

    moveCamera(): void {
        var normalizedLookVector = vec3.create();
        vec3.normalize(normalizedLookVector, this.lookDirection);

        var right = vec3.create();
        vec3.cross(right, normalizedLookVector, vec3.fromValues(0, 1, 0));
        var up = vec3.create();
        vec3.cross(up, normalizedLookVector, right);

        var input: Input = MAIN.webGLApp.input;
        var movementDirection = vec3.create();
        
        if (input.pressed.w) {
            vec3.scale(movementDirection, this.lookDirection, this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.s) {
            vec3.scale(movementDirection, this.lookDirection, -this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.d) {
            vec3.scale(movementDirection, right, this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.a) {
            vec3.scale(movementDirection, right, -this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.q) {
            vec3.scale(movementDirection, up, this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.e) {
            vec3.scale(movementDirection, up, -this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }

        if (this.position[1] < 4) {
            this.position[1] = 4;
        }
    }

    rotateCamera(): void {
        // Set a default value for mouseDelta;
        var mouseDelta: vec2 = vec2.fromValues(0, 0);

        // Rather than return while NOT locked, we just accept mouseMovement when we ARE locked.
        if (MAIN.webGLApp.isLocked) {
            var mouseDelta: vec2 = MAIN.webGLApp.input.mouseDelta;
        }

        this.yaw += mouseDelta[0] * this.rotateSpeed;
        this.pitch -= mouseDelta[1] * this.rotateSpeed;
        // Adjust for static class.
        this.pitch = MathUtils.CLAMP(this.pitch, -1.5, 1.5);

        this.lookDirection[0] = Math.cos(this.pitch) * Math.cos(this.yaw);
        this.lookDirection[1] = Math.sin(this.pitch);
        this.lookDirection[2] = Math.cos(this.pitch) * Math.sin(this.yaw);
    }

    updateViewport(): void {
        MAIN.canvas.width = MAIN.canvas.clientWidth;
        MAIN.canvas.height = MAIN.canvas.clientHeight;
        MAIN.gl.viewport(0, 0, MAIN.canvas.clientWidth, MAIN.canvas.clientHeight);
    }
}