import { SceneObject } from "./transforms/sceneObject";
import { Camera } from "./camera";
import { Mesh } from "./mesh";
import { ShaderObject } from "./shaders/shaderObject";
import { TextureManager } from "./textureManager";
import { ShaderLightGizmo } from "./shaders/shaderLightGizmo";
import { Input } from "./input";
import { AmbientLight } from "./transforms/ambientLight";
import { MAIN } from "./main";
import { Time } from "./utils/time";
import { ShaderGrid } from "./shaders/shaderGrid";
import { Grid } from "./transforms/grid";
import { vec3 } from "gl-matrix";
import { ShaderBullet } from "./shaders/shaderBullet";
import { Bullet } from "./transforms/bullet";
import { MathUtils } from "./utils/mathUtils";
import { Explosion } from "./transforms/explosion";

export class WebGLApp {
    constructor() {
    }
    
    static APP : WebGLApp;
    
    point           : Mesh;
    cube            : Mesh;
    pyramid         : Mesh;
    sphere          : Mesh;
    sceneObjects    : SceneObject[];
    grid            : Grid;
    textureManager  : TextureManager;
    shaderObject    : ShaderObject;
    shaderLightGizmo: ShaderLightGizmo;
    shaderGrid      : ShaderGrid;
    shaderBullet    : ShaderBullet;
    camera          : Camera;
    ambientLight    : AmbientLight;
    input           : Input        = new Input();
    isLocked        : boolean      = false;
    time            : Time         = new Time();
    bullets         : Bullet[]     = [];
    explosions      : Explosion[]  = [];
    target          : SceneObject;
    outputElement   : HTMLElement | null;
    hits            : number = 0;

    initialize() {
        this.lockCursor();
        this.setWebGL2Preferences();
        
        WebGLApp.APP          = this;
        this.shaderObject     = new ShaderObject();
        this.shaderLightGizmo = new ShaderLightGizmo();
        this.shaderGrid       = new ShaderGrid();
        this.shaderBullet     = new ShaderBullet();
        this.point            = new Mesh(Mesh.POINT);
        this.cube             = new Mesh(Mesh.CUBE);
        this.pyramid          = new Mesh(Mesh.PYRAMID);
        this.sphere           = new Mesh(Mesh.SPHERE);
        this.camera           = new Camera();
        this.ambientLight     = new AmbientLight();
        this.grid             = new Grid();
        this.outputElement    = document.getElementById('output');
        
        this.sceneObjects = [
            new SceneObject(this.cube, vec3.create(), vec3.create(), vec3.fromValues(2, 2, 2))
        ,   new SceneObject(this.cube)
        ,   new SceneObject(this.cube, vec3.fromValues(-74, 10, -74), vec3.create(), vec3.fromValues(20, 20, 20))
        ,   new SceneObject(this.pyramid, vec3.fromValues(74, 10, -74), vec3.create(), vec3.fromValues(20, 20, 20))
        ,   new SceneObject(this.cube, vec3.fromValues(74, 10, 74), vec3.create(), vec3.fromValues(20, 20, 20))
        ,   new SceneObject(this.pyramid, vec3.fromValues(-74, 10, 74), vec3.create(), vec3.fromValues(20, 20, 20))
        ];

        this.target = this.sceneObjects[0];
        this.target.offsetSpeed = .1;
        this.target.setIsRandomMove();
        this.target.position[0] = 5;

        for (let i = 2; i < this.sceneObjects.length; i++) {
            this.sceneObjects[i].setIsSpasm();
        }

        this.textureManager = new TextureManager();

        requestAnimationFrame(() => { this.update() });

        document.addEventListener('mousedown', (e: MouseEvent) => {
            this.shoot();
        })
    }

    setWebGL2Preferences() {
        MAIN.gl.viewport(0, 0, MAIN.canvas.clientWidth, MAIN.canvas.clientHeight);
        MAIN.gl.clearColor(0, 0, 0, 1);
        MAIN.gl.enable(MAIN.gl.DEPTH_TEST);
    }

    update() {
        if (!this.textureManager.isReady()) {
            requestAnimationFrame(() => { this.update() });
            return;
        }

        this.draw();

        this.time.update();
        this.grid.update();
        this.camera.update();

        this.sceneObjects.forEach(el => {
            el.update();
        });

        this.trackBullets();
        this.trackExplosions();

        // Highlight grid.
        MAIN.gl.useProgram(MAIN.webGLApp.shaderGrid.program);
        MAIN.gl.uniform3fv(MAIN.webGLApp.shaderGrid.propertyLocationWorldPosition, this.target.position);

        this.ambientLight.update();

        this.input.reset();
        
        requestAnimationFrame(() => { this.update() });
    }

    draw() {
        MAIN.gl.clear(MAIN.gl.COLOR_BUFFER_BIT | MAIN.gl.DEPTH_BUFFER_BIT);
    }

    lockCursor() {
        var canvas = MAIN.canvas;

        canvas.onclick = () => {
            canvas.requestPointerLock();
        };

        canvas.requestPointerLock();
        
        document.addEventListener('pointerlockchange', () => {
            this.isLocked = (document.pointerLockElement == canvas);
        }, false);
    }

    shoot() {
        let spawnPosition : vec3 = vec3.create();
        vec3.copy(spawnPosition, this.camera.position);
        let lookDirection : vec3 = vec3.create();
        vec3.add(spawnPosition, spawnPosition, vec3.scale(lookDirection, this.camera.lookDirection, 3));

        const newBullet = new Bullet(WebGLApp.APP.cube, spawnPosition, vec3.create(), vec3.fromValues(1, 1, 1));
        vec3.copy(newBullet.direction, this.camera.lookDirection);
        this.bullets.push(newBullet);
    }

    private trackBullets() {
        let disposeBullets: Bullet[] = [];
        let min = this.target.min;
        let max = this.target.max;
        
        this.bullets.forEach(el => {
            el.update();

            // let targetPosition = this.target.position;
            // let isHit: boolean = MathUtils.DISTANCE(el.position, targetPosition) < 2.8;
            let isHit : boolean = MathUtils.IS_INSIDE_BOX(el.position, min, max);
            isHit = MathUtils.IS_BOX_OVERLAP(this.target.min, this.target.max, el.min, el.max);

            let isDead: boolean = el.lifetime > el.lifespan;
            if (isDead || isHit) {
                if (isHit) {
                    this.hits++;

                    if (this.outputElement) {
                        this.outputElement.innerText = `hits: ${this.hits}`;
                    }

                    this.target.offsetSpeed += .05;
                    this.explosions.push(new Explosion(WebGLApp.APP.sphere, el.position));
                }

                disposeBullets.push(el);
            }
        });

        disposeBullets.forEach(bullet => {
            this.bullets.splice(this.bullets.indexOf(bullet), 1);
        });
    }

    private trackExplosions() {
        let disposeExplosions: Explosion[] = [];

        this.explosions.forEach(el => {
            el.update();

            let isDead: boolean = el.lifetime > el.lifespan;
            if (isDead) {
                disposeExplosions.push(el);
            }
        });

        disposeExplosions.forEach(explosion => {
            this.explosions.splice(this.explosions.indexOf(explosion), 1);
        });
    }
}