import { vec2 } from "gl-matrix";

export class Input {
    constructor() {
        this.setKeyboardListener();
    }

    input       : vec2 = vec2.create();
    mouseDelta  : vec2 = vec2.create();
    pressed = {
        w: false,
        a: false,
        s: false,
        d: false,
        q: false,
        e: false,
        mouseClick: false
    }

    setKeyboardListener(): void {
        document.addEventListener("keydown", (e: KeyboardEvent) => {
            if (e.key == 'd' || e.keyCode.toString() == '39')
                this.pressed.d = true;
            if (e.key == 'a' || e.keyCode.toString() == '37')
                this.pressed.a = true;
            if (e.key == 'w' || e.keyCode.toString() == '38')
                this.pressed.w = true;
            if (e.key == 's' || e.keyCode.toString() == '40')
                this.pressed.s = true;
            if (e.key == 'q')
                this.pressed.q = true;
            if (e.key == 'e')
                this.pressed.e = true;
        });
        document.addEventListener("keyup", (e: KeyboardEvent) => {
            if (e.key == 'd' || e.keyCode.toString() == '39')
                this.pressed.d = false;
            if (e.key == 'a' || e.keyCode.toString() == '37')
                this.pressed.a = false;
            if (e.key == 'w' || e.keyCode.toString() == '38')
                this.pressed.w = false;
            if (e.key == 's' || e.keyCode.toString() == '40')
                this.pressed.s = false;
            if (e.key == 'q')
                this.pressed.q = false;
            if (e.key == 'e')
                this.pressed.e = false;
        });
        document.addEventListener('mousemove', (e: MouseEvent) => {
            this.mouseDelta = vec2.fromValues(e.movementX, e.movementY);
        });
    }

    reset() {
        this.mouseDelta = vec2.fromValues(0, 0);
    }
}