import { MAIN } from "../main";

export class Shader {
    constructor(sourceVertex: string, sourceFragment: string) {
        this.program = this.getNewShaderProgram(sourceVertex, sourceFragment);
    }

    program: WebGLProgram;
    propertyLocationPosition        : number;
    propertyLocationModelMatrix     : WebGLUniformLocation;
    propertyLocationProjectionMatrix: WebGLUniformLocation;
    propertyLocationViewMatrix      : WebGLUniformLocation;

    getNewShaderProgram(_sourceVertex: string, _sourceFragment: string): WebGLProgram {
        var vertexShader  : WebGLShader = <WebGLShader>this.getAndCompileShader(_sourceVertex, MAIN.gl.VERTEX_SHADER);
        var fragmentShader: WebGLShader = <WebGLShader>this.getAndCompileShader(_sourceFragment, MAIN.gl.FRAGMENT_SHADER);
    
        var newShaderProgram: WebGLProgram = <WebGLProgram>MAIN.gl.createProgram();
        MAIN.gl.attachShader(newShaderProgram, vertexShader);
        MAIN.gl.attachShader(newShaderProgram, fragmentShader);
        MAIN.gl.linkProgram(newShaderProgram);
        MAIN.gl.useProgram(newShaderProgram);
    
        return newShaderProgram;
    }

    getAndCompileShader(_shaderSource: string, _shaderType: number) {
        var newShader: WebGLShader = <WebGLShader>MAIN.gl.createShader(_shaderType);
        MAIN.gl.shaderSource(newShader, _shaderSource);
        MAIN.gl.compileShader(newShader);
    
        if (!MAIN.gl.getShaderParameter(newShader, MAIN.gl.COMPILE_STATUS)) {
            alert(MAIN.gl.getShaderInfoLog(newShader));
            return null;
        }
    
        return newShader;
    }

    getPropertyLocations() {
        MAIN.gl.useProgram(this.program);

        this.propertyLocationModelMatrix      = <WebGLUniformLocation>MAIN.gl.getUniformLocation(this.program, "modelMatrix");
        this.propertyLocationProjectionMatrix = <WebGLUniformLocation>MAIN.gl.getUniformLocation(this.program, "projectionMatrix");
        this.propertyLocationViewMatrix       = <WebGLUniformLocation>MAIN.gl.getUniformLocation(this.program, "viewMatrix");
    }
}