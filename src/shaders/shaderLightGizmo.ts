import { MAIN } from "../main";
import { Shader } from "./shader";

const VERTEX_SOURCE_LIGHT_GIZMO = 
    `#version 300 es
    precision mediump float;
    
    in vec3 position;
    
    uniform mat4 modelMatrix;
    uniform mat4 viewMatrix;
    uniform mat4 projectionMatrix;
    
    void main() {
        vec4 pos = vec4(position, 1.0);
        gl_Position = projectionMatrix * viewMatrix * modelMatrix * pos;
    }`
;
const FRAGMENT_SOURCE_LIGHT_GIZMO =
    `#version 300 es
    precision mediump float;

    uniform vec3 color;
    uniform float intensity;
    
    out vec4 outColor;
    
    void main() {
        outColor = vec4(color * intensity, 1);
    }`
;

export class ShaderLightGizmo extends Shader {
    constructor() {
        super(VERTEX_SOURCE_LIGHT_GIZMO, FRAGMENT_SOURCE_LIGHT_GIZMO);
        this.getPropertyLocations();
    }

    propertyLocationColor    : WebGLUniformLocation;
    propertyLocationIntensity: WebGLUniformLocation;

    getPropertyLocations() {
        super.getPropertyLocations();

        MAIN.gl.useProgram(this.program);

        this.propertyLocationPosition         = MAIN.gl.getAttribLocation(this.program, "position");
        
        this.propertyLocationColor            = <WebGLUniformLocation>MAIN.gl.getUniformLocation(this.program, "color");
        this.propertyLocationIntensity        = <WebGLUniformLocation>MAIN.gl.getUniformLocation(this.program, "intensity");

        MAIN.gl.enableVertexAttribArray(this.propertyLocationPosition);
    }
}