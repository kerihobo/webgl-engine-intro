import { Shader } from "./shader";
import { MAIN } from "../main";

const VERTEX_SOURCE_GRID = 
    `#version 300 es
    
    uniform mat4 modelMatrix;
    uniform mat4 viewMatrix;
    uniform mat4 projectionMatrix;
    uniform int dimensions;
    uniform float time;
    uniform vec3 worldPosition;
    
    out vec3 linkedColor;
    
    void main() {
        float pi = 3.14159265358;
        int d = dimensions;
        
        // vec3 pos = vec3(gl_VertexID % d, 0, ((gl_VertexID % d) - gl_VertexID) / d);
        vec3 pos = vec3(gl_VertexID % d, 0, gl_VertexID / d);
        pos.xz -= (float(d) / 2.0) - .5;

        // One cool way.
        float twirlSpeed = 1.0;
        float frequency = .4;
        float phase = sin(time * twirlSpeed) * length(pos.xz) * frequency;
        pos.xz += normalize(vec2(sin(phase), cos(phase)));

        float speed = 5.0;
        pos.y = sin(pos.x + (time * speed) - cos(pos.z + (time * speed * 0.8))) * cos(pos.z + (time * speed * 0.8)) * .5;

        gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(pos, 1);

        float objDist = distance(worldPosition, pos);
        bool isInRange = objDist < 5.0;
        gl_PointSize = isInRange ? 10.0 * mix(1.5, 0.1, objDist / 5.0) : 2.0;

        vec3 distanceColor = mix(vec3(1, 1, 0), vec3(1, 0, 0), objDist / 5.0);
        vec3 gridColor = mix(vec3(0.2, 0.2, 0.8), vec3(1.0, 1.0, 1.0), (pos.y - -0.5) / (0.5 - -0.5));
        linkedColor = (isInRange ? distanceColor : gridColor);
    }`
;
const FRAGMENT_SOURCE_GRID =
    `#version 300 es
    precision mediump float;

    in vec3 linkedColor;

    out vec4 outColor;
    
    void main() {
        outColor = vec4(linkedColor, 1);
    }`
;

export class ShaderGrid extends Shader {
    constructor() {
        super(VERTEX_SOURCE_GRID, FRAGMENT_SOURCE_GRID);
        
        this.getPropertyLocations();
    }
    
    propertyLocationDimensions: WebGLUniformLocation;
    // Location of time.
    propertyLocationTime: WebGLUniformLocation;
    // Add worldPosition
    propertyLocationWorldPosition: WebGLUniformLocation;
    
    getPropertyLocations() {
        super.getPropertyLocations();
        
        MAIN.gl.useProgram(this.program);
    
        this.propertyLocationDimensions    = <WebGLUniformLocation>MAIN.gl.getUniformLocation(this.program, "dimensions");
        // Get location of time.
        this.propertyLocationTime          = <WebGLUniformLocation>MAIN.gl.getUniformLocation(this.program, "time");
        // Get location of worldPosition.
        this.propertyLocationWorldPosition = <WebGLUniformLocation>MAIN.gl.getUniformLocation(this.program, "worldPosition");
    }
}