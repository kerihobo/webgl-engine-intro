import { Shader } from "./shader";
import { MAIN } from "../main";

const VERTEX_SOURCE_BULLET = 
    `#version 300 es
    
    in vec4 position;
    
    uniform mat4 modelMatrix;
    uniform mat4 viewMatrix;
    uniform mat4 projectionMatrix;
    
    void main() {
        gl_Position = projectionMatrix * viewMatrix * modelMatrix * position;
        gl_PointSize = 10.0;
    }`
;
const FRAGMENT_SOURCE_BULLET =
    `#version 300 es
    precision mediump float;

    out vec4 outColor;
    
    void main() {
        outColor = vec4(1, 1, 1, 1);
    }`
;

export class ShaderBullet extends Shader {
    constructor() {
        super(VERTEX_SOURCE_BULLET, FRAGMENT_SOURCE_BULLET);
        this.getPropertyLocations();
    }
    
    getPropertyLocations() {
        super.getPropertyLocations();
        
        MAIN.gl.useProgram(this.program);
        
        this.propertyLocationPosition = MAIN.gl.getAttribLocation(this.program, "position");
        MAIN.gl.enableVertexAttribArray(this.propertyLocationPosition);
    }
}