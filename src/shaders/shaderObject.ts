import { MAIN } from "../main";
import { Shader } from "./shader";

const VERTEX_SOURCE_OBJECT = 
    `#version 300 es
    precision mediump float;
    
    in vec4 position;
    in vec4 color;
    in float pointSize;
    in vec2 texCoord;
    in vec3 vertexNormal;

    uniform mat4 projectionMatrix;
    uniform mat4 viewMatrix;
    uniform mat4 modelMatrix;

    out vec4 linkedColor;
    out vec2 linkedTexCoord;
    out vec4 linkedVertexWorldPosition;
    out vec3 linkedVertexNormal;

    void main() {
        gl_Position = projectionMatrix * viewMatrix * modelMatrix * position;
        gl_PointSize = pointSize;
        linkedColor = color;
        linkedTexCoord = texCoord;
        linkedVertexWorldPosition = modelMatrix * position;

        // linkedVertexNormal = mat3(modelMatrix) * vertexNormal;
        linkedVertexNormal = mat3(inverse(transpose(modelMatrix))) * vertexNormal;
    }`
;
const FRAGMENT_SOURCE_OBJECT =
    `#version 300 es
    precision mediump float;

    in vec4 linkedColor;
    in vec2 linkedTexCoord;
    in vec4 linkedVertexWorldPosition;
    in vec3 linkedVertexNormal;

    uniform sampler2D texture0;
    uniform sampler2D texture1;
    uniform vec3 ambientColor;
    uniform float ambientIntensity;
    uniform vec3 lightPosition;
    uniform vec3 cameraPosition;

    out vec4 outColor;

    void main() {
        vec3 lightDirection = normalize(lightPosition - vec3(linkedVertexWorldPosition));
        vec3 viewDirection = normalize(cameraPosition - vec3(linkedVertexWorldPosition));
        vec3 normalizedNormal = normalize(linkedVertexNormal);

        float diffuseIntensity = max(dot(normalizedNormal, lightDirection), 0.0);
        vec4 diffuseComponent = vec4(diffuseIntensity * ambientColor, 1);

        vec4 ambientComponent = vec4(ambientIntensity * ambientColor, 1);

        vec4 colorComponent = mix(texture(texture0, linkedTexCoord), texture(texture1, linkedTexCoord), 0.5) * linkedColor;

        vec3 reflectedLightDirection = reflect(-lightDirection, normalizedNormal);
        float specularIntensity = pow(max(dot(reflectedLightDirection, viewDirection), 0.0f), 16.0f);
        float specularIntensityMultiplier = 5.0;
        vec4 specularComponent = vec4(((specularIntensity * specularIntensityMultiplier) * ambientColor), 1.0f);

        // outColor = colorComponent * (ambientComponent + diffuseComponent + specularComponent);
        outColor = colorComponent * (ambientComponent + diffuseComponent + (specularComponent * texture(texture0, linkedTexCoord).r));
    }`
;

export class ShaderObject extends Shader {
    constructor() {
        super(VERTEX_SOURCE_OBJECT, FRAGMENT_SOURCE_OBJECT);
        this.getPropertyLocations();
    }

    propertyLocationPointSize       : number;
    propertyLocationColor           : number;
    propertyLocationTexCoord        : number;
    propertyLocationVertexNormal    : number;
    propertyLocationLightPosition   : WebGLUniformLocation;
    propertyLocationCameraPosition  : WebGLUniformLocation;
    propertyLocationAmbientColor    : WebGLUniformLocation;
    propertyLocationAmbientIntensity: WebGLUniformLocation;
    propertyLocationTextures        : WebGLUniformLocation[];
    
    getPropertyLocations() {
        super.getPropertyLocations();
        
        MAIN.gl.useProgram(this.program);

        this.propertyLocationPosition         = MAIN.gl.getAttribLocation(this.program, "position");
        this.propertyLocationPointSize        = MAIN.gl.getAttribLocation(this.program, "pointSize");
        this.propertyLocationColor            = MAIN.gl.getAttribLocation(this.program, "color");
        this.propertyLocationTexCoord         = MAIN.gl.getAttribLocation(this.program, "texCoord");
        this.propertyLocationVertexNormal     = MAIN.gl.getAttribLocation(this.program, "vertexNormal");

        this.propertyLocationLightPosition    = <WebGLUniformLocation>MAIN.gl.getUniformLocation(this.program, "lightPosition");
        this.propertyLocationCameraPosition    = <WebGLUniformLocation>MAIN.gl.getUniformLocation(this.program, "cameraPosition");
        this.propertyLocationAmbientColor     = <WebGLUniformLocation>MAIN.gl.getUniformLocation(this.program, "ambientColor");
        this.propertyLocationAmbientIntensity = <WebGLUniformLocation>MAIN.gl.getUniformLocation(this.program, "ambientIntensity");
        this.propertyLocationTextures = [
            <WebGLUniformLocation>MAIN.gl.getUniformLocation(this.program  , "texture0")
        ,   <WebGLUniformLocation>MAIN.gl.getUniformLocation(this.program  , "texture1")
        ]

        MAIN.gl.enableVertexAttribArray(this.propertyLocationPosition);
        MAIN.gl.enableVertexAttribArray(this.propertyLocationColor);
        MAIN.gl.enableVertexAttribArray(this.propertyLocationTexCoord);
        MAIN.gl.enableVertexAttribArray(this.propertyLocationVertexNormal);
    }
}