import { vec3, vec2 } from "gl-matrix";

export class MathUtils {
    constructor() {
    }

    static DEG_TO_RAD(_angle: number): number{
        return _angle * (Math.PI / 180.0);
    }

    static RAD_TO_DEG(_angle: number): number {
        return (_angle / Math.PI) * 180.0;
    }

    static CLAMP(_value: number, _min: number, _max: number): number {
        if (_value < _min) {
            return _min;
        }

        if (_value > _max) {
            return _max;
        }

        return _value;
    }

    static LERP(_a: number, _b: number, _t: number): number {
        return (1 - _t) * _a + _t * _b;
    }

    static INVERSE_LERP(_a: number, _b: number, _t: number): number {
        return (_t - _a) / (_b - _a);
    }

    static DISTANCE(_a: vec3, _b: vec3): number {
        var difference: vec3 = vec3.create();
        difference[0] = Math.abs(_b[0] - _a[0]);
        difference[1] = Math.abs(_b[1] - _a[1]);
        difference[2] = Math.abs(_b[2] - _a[2]);

        return MathUtils.NTH_ROOT(
            Math.pow(difference[0], 3)
        +   Math.pow(difference[1], 3)
        +   Math.pow(difference[2], 3)
        ,   3
        );
    }

    static NTH_ROOT(_value: number, n: number): number {
        return Math.pow(_value, 1/n);
    }

    // Add remap.
    static REMAP(_value: number, _inMinMax: vec2, _ouMinMax: vec2) {
        return _ouMinMax[0] + (_value - _inMinMax[0]) * (_ouMinMax[1] - _ouMinMax[0]) / (_inMinMax[1] - _inMinMax[0]);
    }

    static VEC3_SCALE(_a: vec3, _b: number) {
        let newVec3 : vec3 = vec3.create();
        vec3.copy(newVec3, _a);
        
        newVec3[0] *= _b;
        newVec3[1] *= _b;
        newVec3[2] *= _b;

        return newVec3;
    }

    static IS_INSIDE_BOX(point: vec3, min: vec3, max: vec3): boolean {
        return (
            point[0] >= min[0] &&
            point[0] <= max[0] &&
            point[1] >= min[1] &&
            point[1] <= max[1] &&
            point[2] >= min[2] &&
            point[2] <= max[2]
        );
    }

    static IS_BOX_OVERLAP(_minA: vec3, _maxA: vec3, _minB: vec3, _maxB: vec3): boolean {
        for (let i = 0; i < 3; i++) {
            if (_maxA[i] < _minB[i] || _minA[i] > _maxB[i]) {
                return false; // No overlap along this axis
            }
        }
        return true; // Overlapping along all axes
    }
}