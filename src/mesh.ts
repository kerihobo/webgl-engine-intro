import { MAIN } from "./main";

export class Mesh {
    constructor(_vertexArray: number[]) {
        this.vertexCount = _vertexArray.length / (Mesh.STRIDE / Mesh.FLOAT_SIZE_BYTES);
        this.createVertices(_vertexArray);
    }

    static readonly FLOAT_SIZE_BYTES = 4;
    static readonly STRIDE = 12 * this.FLOAT_SIZE_BYTES;
    static readonly VERTEX_COLOR_OFFSET = 3 * this.FLOAT_SIZE_BYTES;
    static readonly VERTEX_TEX_COORDS_OFFSET = 7 * this.FLOAT_SIZE_BYTES;
    static readonly VERTEX_NORMALS_OFFSET = 9 * this.FLOAT_SIZE_BYTES;
    
    vertexArray: number[];
    vertexCount: number;

    static POINT: number[] = [
        .0, .0, .0,     1.0, 1.0, 1.0, 1.0,     .0, .0,        .0,  .0, -1.0
    ];

    static CUBE: number[] = [
        -0.5, -0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     0.0, 0.0,        0.0,  0.0, -1.0,
         0.5, -0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     1.0, 0.0,        0.0,  0.0, -1.0,
         0.5,  0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     1.0, 1.0,        0.0,  0.0, -1.0,
         0.5,  0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     1.0, 1.0,        0.0,  0.0, -1.0,
        -0.5,  0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     0.0, 1.0,        0.0,  0.0, -1.0,
        -0.5, -0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     0.0, 0.0,        0.0,  0.0, -1.0,
        
        -0.5, -0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     0.0, 0.0,        0.0,  0.0,  1.0,
         0.5, -0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     1.0, 0.0,        0.0,  0.0,  1.0,
         0.5,  0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     1.0, 1.0,        0.0,  0.0,  1.0,
         0.5,  0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     1.0, 1.0,        0.0,  0.0,  1.0,
        -0.5,  0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     0.0, 1.0,        0.0,  0.0,  1.0,
        -0.5, -0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     0.0, 0.0,        0.0,  0.0,  1.0,
        
        -0.5,  0.5,  0.5,       0.0, 0.0, 1.0, 1.0,     1.0, 0.0,       -1.0,  0.0,  0.0,
        -0.5,  0.5, -0.5,       0.0, 0.0, 1.0, 1.0,     1.0, 1.0,       -1.0,  0.0,  0.0,
        -0.5, -0.5, -0.5,       0.0, 0.0, 1.0, 1.0,     0.0, 1.0,       -1.0,  0.0,  0.0,
        -0.5, -0.5, -0.5,       0.0, 0.0, 1.0, 1.0,     0.0, 1.0,       -1.0,  0.0,  0.0,
        -0.5, -0.5,  0.5,       0.0, 0.0, 1.0, 1.0,     0.0, 0.0,       -1.0,  0.0,  0.0,
        -0.5,  0.5,  0.5,       0.0, 0.0, 1.0, 1.0,     1.0, 0.0,       -1.0,  0.0,  0.0,
        
         0.5,  0.5,  0.5,       1.0, 1.0, 0.0, 1.0,     0.0, 1.0,        1.0,  0.0,  0.0,
         0.5,  0.5, -0.5,       1.0, 1.0, 0.0, 1.0,     1.0, 1.0,        1.0,  0.0,  0.0,
         0.5, -0.5, -0.5,       1.0, 1.0, 0.0, 1.0,     1.0, 0.0,        1.0,  0.0,  0.0,
         0.5,  0.5,  0.5,       1.0, 1.0, 0.0, 1.0,     0.0, 1.0,        1.0,  0.0,  0.0,
         0.5, -0.5,  0.5,       1.0, 1.0, 0.0, 1.0,     0.0, 0.0,        1.0,  0.0,  0.0,
         0.5, -0.5, -0.5,       1.0, 1.0, 0.0, 1.0,     1.0, 0.0,        1.0,  0.0,  0.0,
        
        -0.5, -0.5, -0.5,       0.0, 1.0, 1.0, 1.0,     0.0, 1.0,        0.0, -1.0,  0.0,
         0.5, -0.5, -0.5,       0.0, 1.0, 1.0, 1.0,     1.0, 1.0,        0.0, -1.0,  0.0,
         0.5, -0.5,  0.5,       0.0, 1.0, 1.0, 1.0,     1.0, 0.0,        0.0, -1.0,  0.0,
         0.5, -0.5,  0.5,       0.0, 1.0, 1.0, 1.0,     1.0, 0.0,        0.0, -1.0,  0.0,
        -0.5, -0.5,  0.5,       0.0, 1.0, 1.0, 1.0,     0.0, 0.0,        0.0, -1.0,  0.0,
        -0.5, -0.5, -0.5,       0.0, 1.0, 1.0, 1.0,     0.0, 1.0,        0.0, -1.0,  0.0,
        
        -0.5,  0.5, -0.5,       1.0, 0.0, 1.0, 1.0,     0.0, 1.0,        0.0,  1.0,  0.0,
         0.5,  0.5, -0.5,       1.0, 0.0, 1.0, 1.0,     1.0, 1.0,        0.0,  1.0,  0.0,
         0.5,  0.5,  0.5,       1.0, 0.0, 1.0, 1.0,     1.0, 0.0,        0.0,  1.0,  0.0,
         0.5,  0.5,  0.5,       1.0, 0.0, 1.0, 1.0,     1.0, 0.0,        0.0,  1.0,  0.0,
        -0.5,  0.5,  0.5,       1.0, 0.0, 1.0, 1.0,     0.0, 0.0,        0.0,  1.0,  0.0,
        -0.5,  0.5, -0.5,       1.0, 0.0, 1.0, 1.0,     0.0, 1.0,        0.0,  1.0,  0.0
    ];

    static PYRAMID: number[] = [
        -0.5, -0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     0.0, 0.0,        0.0,  1.0, -1.0, // back
         0.5, -0.5, -0.5,       0.0, 1.0, 0.0, 1.0,     1.0, 0.0,        0.0,  1.0, -1.0,
         0.0,  0.5,  0.0,       0.0, 0.0, 1.0, 1.0,     0.5, 1.0,        0.0,  1.0, -1.0,

        -0.5, -0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     0.0, 0.0,        0.0,  1.0,  1.0, // forward
         0.5, -0.5,  0.5,       1.0, 0.0, 0.0, 1.0,     1.0, 0.0,        0.0,  1.0,  1.0,
         0.0,  0.5,  0.0,       0.0, 0.0, 1.0, 1.0,     0.5, 1.0,        0.0,  1.0,  1.0,

        -0.5, -0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     0.0, 0.0,       -1.0,  1.0,  0.0, // left
        -0.5, -0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     1.0, 0.0,       -1.0,  1.0,  0.0,
         0.0,  0.5,  0.0,       0.0, 0.0, 1.0, 1.0,     0.5, 1.0,       -1.0,  1.0,  0.0,

         0.5, -0.5,  0.5,       1.0, 0.0, 0.0, 1.0,     0.0, 0.0,        1.0,  1.0,  0.0, // right
         0.5, -0.5, -0.5,       0.0, 1.0, 0.0, 1.0,     1.0, 0.0,        1.0,  1.0,  0.0,
         0.0,  0.5,  0.0,       0.0, 0.0, 1.0, 1.0,     0.5, 1.0,        1.0,  1.0,  0.0,

        -0.5, -0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     0.0, 0.0,        0.0, -1.0,  0.0, // down
        -0.5, -0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     0.0, 1.0,        0.0, -1.0,  0.0,
         0.5, -0.5,  0.5,       1.0, 0.0, 0.0, 1.0,     1.0, 1.0,        0.0, -1.0,  0.0,

        -0.5, -0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     0.0, 0.0,        0.0, -1.0,  0.0, // down
         0.5, -0.5,  0.5,       1.0, 0.0, 0.0, 1.0,     1.0, 1.0,        0.0, -1.0,  0.0,
         0.5, -0.5, -0.5,       0.0, 1.0, 0.0, 1.0,     1.0, 0.0,        0.0, -1.0,  0.0,
    ];

    static SPHERE : number[] = this.createSphere(1.0, 30, 30);
    
    // Usage example
    // let sphereVertexArray = createSphereVertexArray(1.0, 30, 30);

    vertexDataBuffer: WebGLBuffer = <WebGLBuffer>MAIN.gl.createBuffer();

    createVertices(_vertexArray: number[]) {
        this.vertexArray = _vertexArray;
        
        MAIN.gl.bindBuffer(MAIN.gl.ARRAY_BUFFER, this.vertexDataBuffer);
        MAIN.gl.bufferData(MAIN.gl.ARRAY_BUFFER, new Float32Array(_vertexArray), MAIN.gl.STATIC_DRAW);
    }

    static createSphere(radius: number, latitudeBands: number, longitudeBands: number) {
        const vertices = []; // Combined array for all attributes
    
        for (let latNumber = 0; latNumber <= latitudeBands; latNumber++) {
            const theta = (latNumber * Math.PI) / latitudeBands;
            const sinTheta = Math.sin(theta);
            const cosTheta = Math.cos(theta);
    
            for (let longNumber = 0; longNumber <= longitudeBands; longNumber++) {
                const phi = (longNumber * 2 * Math.PI) / longitudeBands;
                const sinPhi = Math.sin(phi);
                const cosPhi = Math.cos(phi);
    
                // Vertex position
                const x = radius * cosPhi * sinTheta;
                const y = radius * cosTheta;
                const z = radius * sinPhi * sinTheta;
    
                // Vertex color (red in this example)
                const r = 1.0;
                const g = 0.0;
                const b = 0.0;
                const a = 1.0;
    
                // UV coordinates (normalized)
                const u = longNumber / longitudeBands;
                const v = latNumber / latitudeBands;
    
                // Normal direction (same as position for a sphere)
                const nx = x;
                const ny = y;
                const nz = z;
    
                // Combine all attributes into a single array
                vertices.push(x, y, z, r, g, b, a, u, v, nx, ny, nz);
            }
        }
    
        return vertices;
    }
}