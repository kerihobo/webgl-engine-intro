import { vec3 } from "gl-matrix";
import { MAIN } from "../main";
import { Transform } from "./transform";
import { Time } from "../utils/time";

export class AmbientLight extends Transform {
    constructor(position: vec3 = vec3.fromValues(0, 0, 0), rotation: vec3 = vec3.fromValues(0, 0, 0), scale: vec3 = vec3.fromValues(.33, .33, .33)) {
        super(position, rotation, scale, MAIN.webGLApp.shaderLightGizmo);
        
        this.updateAmbientLight();
    }
    
    color    : vec3   = vec3.fromValues(.33, .33, 1);
    intensity: number = 0.5;
    speed    : vec3   = vec3.fromValues(3, 2.2, 1.7);
    distance : vec3   = vec3.fromValues(1, 1.5, 1);

    updateAmbientLight() {
        MAIN.gl.useProgram(MAIN.webGLApp.shaderObject.program);
        MAIN.gl.uniform3fv(MAIN.webGLApp.shaderObject.propertyLocationLightPosition, this.position);
        MAIN.gl.uniform3fv(MAIN.webGLApp.shaderObject.propertyLocationAmbientColor, this.color);
        MAIN.gl.uniform1f(MAIN.webGLApp.shaderObject.propertyLocationAmbientIntensity, this.intensity);

        MAIN.gl.useProgram(MAIN.webGLApp.shaderLightGizmo.program);
        MAIN.gl.uniform3fv(MAIN.webGLApp.shaderLightGizmo.propertyLocationColor, this.color);
        MAIN.gl.uniform1f(MAIN.webGLApp.shaderLightGizmo.propertyLocationIntensity, this.intensity);
    }

    update() {
        this.transform();
        this.applyTransforms();

        MAIN.gl.useProgram(MAIN.webGLApp.shaderObject.program);
        MAIN.gl.uniform3fv(MAIN.webGLApp.shaderObject.propertyLocationLightPosition, this.position);

        this.draw();
    }

    transform() {
        this.position = vec3.fromValues(
            Math.cos(Time.ELAPSED_TIME * this.speed[0]) * this.distance[0]
        ,   Math.sin(Time.ELAPSED_TIME * this.speed[1]) * this.distance[1]
        ,   Math.cos(Time.ELAPSED_TIME * this.speed[2]) * this.distance[2]
        );
    }

    draw() {
        MAIN.gl.useProgram(MAIN.webGLApp.shaderLightGizmo.program);
        MAIN.gl.bindBuffer(MAIN.gl.ARRAY_BUFFER, MAIN.webGLApp.cube.vertexDataBuffer);

        MAIN.gl.drawArrays(MAIN.gl.TRIANGLES, 0, MAIN.webGLApp.cube.vertexCount);
    }
}