import { mat4, vec3 } from "gl-matrix";
import { MAIN } from "../main";
import { Shader } from "../shaders/shader";

export class Transform {
    constructor(_position: vec3 = vec3.fromValues(0,0,0), _rotation: vec3 = vec3.fromValues(0,0,0), _scale: vec3 = vec3.fromValues(1,1,1), _shader: Shader) {
        this.shader = _shader;
        // Copy these transforms rather than assigning them to the reference of a temporary variable.
        vec3.copy(this.position, _position);
        vec3.copy(this.rotation, _rotation);
        vec3.copy(this.scale, _scale);
    }
    
    position   : vec3 = vec3.fromValues(0, 0, 0);
    rotation   : vec3 = vec3.fromValues(0, 0, 0);
    scale      : vec3 = vec3.fromValues(1, 1, 1);
    modelMatrix: mat4 = mat4.create();
    shader     : Shader;

    applyTransforms() {
        mat4.identity(this.modelMatrix);
        mat4.translate(this.modelMatrix, this.modelMatrix, this.position);
        mat4.rotateZ(this.modelMatrix, this.modelMatrix, this.rotation[0]);
        mat4.rotateY(this.modelMatrix, this.modelMatrix, this.rotation[1]);
        mat4.rotateX(this.modelMatrix, this.modelMatrix, this.rotation[2]);
        mat4.scale(this.modelMatrix, this.modelMatrix, this.scale);
        
        MAIN.gl.useProgram(this.shader.program);
        MAIN.gl.uniformMatrix4fv(this.shader.propertyLocationModelMatrix, false, this.modelMatrix);
    }
}