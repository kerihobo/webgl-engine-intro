import { Transform } from "./transform";
import { MAIN } from "../main";
import { vec3 } from "gl-matrix";
import { Time } from "../utils/time";
import { Mesh } from "../mesh";
import { ShaderBullet } from "../shaders/shaderBullet";
import { MathUtils } from "../utils/mathUtils";

export class Bullet extends Transform {
    constructor(_mesh: Mesh, _position: vec3 = vec3.fromValues(0,0,0), _rotation: vec3 = vec3.fromValues(0,0,0), _scale: vec3 = vec3.fromValues(1,1,1)) {
        super(_position, _rotation, _scale, MAIN.webGLApp.shaderBullet);
        
        this.mesh = _mesh;
    }
    
    readonly lifespan : number = 5;

    direction      : vec3 = vec3.create();
    mesh           : Mesh;
    min            : vec3    = vec3.create();
    max            : vec3    = vec3.create();
    downwardForce  : number = .5;
    lifetime       : number = 0;
    
    update() {
        this.downwardForce -= Time.DELTA_TIME * 1;
        this.lifetime += Time.DELTA_TIME;

        vec3.subtract(this.min, this.position, MathUtils.VEC3_SCALE(this.scale, .5));
        vec3.add(this.max, this.position, MathUtils.VEC3_SCALE(this.scale, .5));
        
        this.transform();
        this.applyTransforms();
        this.draw();
    }

    draw() {
        MAIN.gl.useProgram(MAIN.webGLApp.shaderBullet.program);
        MAIN.gl.bindBuffer(MAIN.gl.ARRAY_BUFFER, this.mesh.vertexDataBuffer);

        this.setShaderProperties();
        
        MAIN.gl.drawArrays(MAIN.gl.TRIANGLES, 0, this.mesh.vertexCount);
    }

    transform() {
        let dir = vec3.create();
        vec3.copy(dir, this.direction);

        vec3.add(this.position, this.position, vec3.scale(dir, dir, 150 * Time.DELTA_TIME));
        vec3.add(this.position, this.position, vec3.fromValues(0, this.downwardForce, 0));

        this.rotation = vec3.fromValues(
            Time.ELAPSED_TIME * 10 
        ,   Time.ELAPSED_TIME * 15
        ,   Time.ELAPSED_TIME * 25
        );
    }

    setShaderProperties() {
        let shader: ShaderBullet = MAIN.webGLApp.shaderBullet;
        MAIN.gl.useProgram(shader.program);

        MAIN.gl.vertexAttribPointer(shader.propertyLocationPosition, 3, MAIN.gl.FLOAT, false, Mesh.STRIDE, 0);
    }
}