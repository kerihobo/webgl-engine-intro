import { Transform } from "./transform";
import { MAIN } from "../main";
import { vec3 } from "gl-matrix";
import { Time } from "../utils/time";
import { Mesh } from "../mesh";
import { ShaderBullet } from "../shaders/shaderBullet";
import { MathUtils } from "../utils/mathUtils";

export class Explosion extends Transform {
    constructor(_mesh: Mesh, _position: vec3 = vec3.fromValues(0,0,0), _rotation: vec3 = vec3.fromValues(0,0,0), _scale: vec3 = vec3.fromValues(1,1,1)) {
        super(_position, _rotation, _scale, MAIN.webGLApp.shaderBullet);
        
        this.mesh = _mesh;
    }
    
    readonly lifespan : number = .25;

    mesh           : Mesh;
    lifetime       : number = 0;
    
    update() {
        this.lifetime += Time.DELTA_TIME;
        
        this.transform();
        this.applyTransforms();
        this.draw();
    }

    draw() {
        MAIN.gl.useProgram(MAIN.webGLApp.shaderBullet.program);
        MAIN.gl.bindBuffer(MAIN.gl.ARRAY_BUFFER, this.mesh.vertexDataBuffer);

        this.setShaderProperties();
        
        MAIN.gl.drawArrays(MAIN.gl.LINES, 0, this.mesh.vertexCount);
    }

    transform() {
        this.scale = vec3.add(vec3.create(), vec3.fromValues(2, 2, 2), MathUtils.VEC3_SCALE(vec3.fromValues(2, 2, 2), this.lifetime / this.lifespan));
        this.rotation = vec3.fromValues(
            Time.ELAPSED_TIME * MathUtils.LERP(-1, 1, Math.random()) * 5 
        ,   Time.ELAPSED_TIME * MathUtils.LERP(-1, 1, Math.random()) * 5
        ,   Time.ELAPSED_TIME * MathUtils.LERP(-1, 1, Math.random()) * 5
        );
    }

    setShaderProperties() {
        let shader: ShaderBullet = MAIN.webGLApp.shaderBullet;
        MAIN.gl.useProgram(shader.program);

        MAIN.gl.vertexAttribPointer(shader.propertyLocationPosition, 3, MAIN.gl.FLOAT, false, Mesh.STRIDE, 0);
    }
}