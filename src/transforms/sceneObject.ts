import { Transform } from "./transform";
import { MAIN } from "../main";
import { vec3, vec2 } from "gl-matrix";
import { Time } from "../utils/time";
import { Mesh } from "../mesh";
import { ShaderObject } from "../shaders/shaderObject";
import { MathUtils } from "../utils/mathUtils";

export class SceneObject extends Transform {
    constructor(_mesh: Mesh, _position: vec3 = vec3.fromValues(0,0,0), _rotation: vec3 = vec3.fromValues(0,0,0), _scale: vec3 = vec3.fromValues(1,1,1)) {
        super(_position, _rotation, _scale, MAIN.webGLApp.shaderObject);
        
        this.mesh = _mesh;
        this.offset = Math.random() * (256 * 256);
        vec3.copy(this.initialPosition, _position);

        this.setShaderProperties();
    }
    
    mesh           : Mesh;
    angle          : number  = Math.random() * (180 - -180) + -180;
    angleSpeed     : number  = Math.random() * (180 - -180) + -180;
    offset         : number  = 0;
    offsetSpeed    : number  = 1;
    initialPosition: vec3    = vec3.create();
    targetPosition : vec3    = vec3.fromValues(4, .5, 4);
    min            : vec3    = vec3.create();
    max            : vec3    = vec3.create();
    t              : number  = 0;
    isMoveRandomly : boolean = false;
    isSpasm        : boolean;
            
    update() {
        this.offset += Time.DELTA_TIME * this.offsetSpeed;

        vec3.subtract(this.min, this.position, MathUtils.VEC3_SCALE(this.scale, .5));
        vec3.add(this.max, this.position, MathUtils.VEC3_SCALE(this.scale, .5));
        
        this.transform();
        this.applyTransforms();
        this.draw();
    }

    draw() {
        MAIN.gl.useProgram(MAIN.webGLApp.shaderObject.program);
        MAIN.gl.bindBuffer(MAIN.gl.ARRAY_BUFFER, this.mesh.vertexDataBuffer);

        this.setShaderProperties();
        
        MAIN.gl.drawArrays(MAIN.gl.TRIANGLES, 0, this.mesh.vertexCount);
    }

    transform() {
        if (this.isMoveRandomly) {
            this.moveRandomly();
        } else if (this.isSpasm) {
            this.spasm();
        }
    }

    setShaderProperties() {
        let shader: ShaderObject = MAIN.webGLApp.shaderObject;
        MAIN.gl.useProgram(shader.program);

        MAIN.gl.vertexAttribPointer(shader.propertyLocationPosition, 3, MAIN.gl.FLOAT, false, Mesh.STRIDE, 0);
        MAIN.gl.vertexAttribPointer(shader.propertyLocationColor, 4, MAIN.gl.FLOAT, false, Mesh.STRIDE, Mesh.VERTEX_COLOR_OFFSET);
        MAIN.gl.vertexAttribPointer(shader.propertyLocationTexCoord, 2, MAIN.gl.FLOAT, false, Mesh.STRIDE, Mesh.VERTEX_TEX_COORDS_OFFSET);
        MAIN.gl.vertexAttribPointer(shader.propertyLocationVertexNormal, 3, MAIN.gl.FLOAT, false, Mesh.STRIDE, Mesh.VERTEX_NORMALS_OFFSET);
    
        MAIN.gl.vertexAttrib1f(shader.propertyLocationPointSize, 50);
    }
    
    setIsRandomMove() {
        this.isMoveRandomly = true;
    }
    
    private moveRandomly() {
        this.position = vec3.fromValues(
            Math.sin((Math.PI * 2) * this.offset * 0.1) * 53.5
        ,   Math.sin(Time.ELAPSED_TIME * (Math.PI * 4)) * .5
        ,   Math.cos((Math.PI * 0.5) * this.offset * 0.138) * 60.8 * Math.cos(Time.ELAPSED_TIME)
        );
    }

    setIsSpasm() {
        this.isSpasm = true;
    }
    
    private spasm() {
        this.rotation = vec3.fromValues(
            Math.sin((Math.PI * 2) * this.offset * 0.1) * 10.5
        ,   Math.sin((Math.PI * 2) * this.offset * 0.1) * 4.5
        ,   Math.cos((Math.PI * 0.5) * this.offset * 0.138) * 8.8 * Math.cos(Time.ELAPSED_TIME)
        );
        
        let newScale : vec3 = vec3.create();
        vec3.subtract(
            newScale
        ,   vec3.fromValues(20, 20, 20)
        ,   vec3.fromValues(
                Math.sin((Math.PI * 2) * this.offset * 0.1) * 10
            ,   Math.sin((Math.PI * 2) * this.offset * 0.1) * 10
            ,   Math.cos((Math.PI * 0.5) * this.offset * 0.138) * 10 * Math.cos(Time.ELAPSED_TIME))
        );

        this.scale = newScale;
    }
}