import { Transform } from "./transform";
import { MAIN } from "../main";
import { vec3 } from "gl-matrix";
import { ShaderGrid } from "../shaders/shaderGrid";
import { Time } from "../utils/time";

export class Grid extends Transform {
    constructor() {
        super(vec3.fromValues(0,0,0), vec3.fromValues(0,0,0), vec3.fromValues(1,1,1), MAIN.webGLApp.shaderGrid);
        
        this.dimensions += 1;
    }

    // Expand our grid as it's kindof our canvas right now.
    dimensions: number = 128;

    update() {
        this.applyTransforms();

        this.draw();
    }

    draw() {
        MAIN.gl.useProgram(MAIN.webGLApp.shaderGrid.program);
        MAIN.gl.uniform1i((<ShaderGrid>this.shader).propertyLocationDimensions, this.dimensions);
        // Send time to the shader.
        MAIN.gl.uniform1f((<ShaderGrid>this.shader).propertyLocationTime, Time.ELAPSED_TIME);
        MAIN.gl.drawArrays(MAIN.gl.POINTS, 0, Math.pow(this.dimensions, 2));
    }
}