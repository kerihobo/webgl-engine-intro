# webgl-engine-intro

Built as a learning project for students simultaneously covering web and game fundamentals. Intended for anyone to expand on if they wish.

Features:
- OOP approach to object management. Add and manipulate objects easily from code. Inherit from Transform class to easily add more objects to your scene without thinking too much about model/view/projection matrices.
- Ambient light, diffuse and specular shading. Examples for manipulating vertices and colours via shaders (see grid).
- Easy workflow for adding more shaders as needed.
- Base for grid-implementation.
- Time class to access elapsed and delta time.
- Example texture-manager to serve as a base when needed for a wider range of uses.
- Spherical and AABB collision detection.
- Object instantiation and lifecycle management at runtime (see explosion).

# Controls

No goal, navigate the scene, shoot the run-away box if you can/want.

WASD       = move left/right, forward/back  
Mouse-move = orient camera  
Left-click = Shoot

Press Esc to get your mouse back.

# Setup

This project is coded in Typescript, it has to be compiled to JS. Just run the following and Webpack will handle the rest.

```
npm i
npm run .
```

This will finally run `http-server`, which will setup the url for you to use, you can either click it or browse to location manually in your browser.